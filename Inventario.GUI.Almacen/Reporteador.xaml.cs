﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Inventario.GUI.Almacen
{
    /// <summary>
    /// Lógica de interacción para Reporteador.xaml
    /// </summary>
    public partial class Reporteador : Window
    {
        string reporte;
        List<ReportDataSource> origenes;
        List<ReportParameter> parametros;
        bool cargado;
        public Reporteador(string nombreReporte, List<ReportDataSource> datos, List<ReportParameter> parametros)
        {
            InitializeComponent();
            reporte = nombreReporte;
            origenes = datos;
            this.parametros = parametros;
            contenedor.Load += Contenedor_Load;
        }

        private void Contenedor_Load(object sender, EventArgs e)
        {
            if (!cargado)
            {
                contenedor.LocalReport.ReportEmbeddedResource = reporte;
                foreach (var item in origenes)
                {
                    contenedor.LocalReport.DataSources.Add(item);
                }
                if (parametros != null)
                {
                    contenedor.LocalReport.SetParameters(parametros);
                }
                contenedor.RefreshReport();
                cargado = true;
            }
        }
    }
}
